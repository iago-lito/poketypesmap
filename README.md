Saturday fun.
These [traditional types maps]
are compact, but difficult to read.
The goal here is to build up
a friendlier, more extensive map.
So this alternate map involves redundancies,
but I expect it is easier to read.

The main script generates a `.pdf` file.
It depends on [`pdflatex`] + [tikz].

To test at home, it should be just a matter of

```
$ git clone --recursive https://gitlab.com/iago-lito/poketypesmap
$ cd poketypesmap
$ ./generate.bash
```

If you are running linux and if everything goes well,
the generated file should then stand in the `output/` folder.

---

### TODO

- Check the map for errors.
- Fix the style, because it is __ugly__ yet, anyone competent?

---

### Cheers

- [@duiker101]'s `.svg` files reproducing the types icons.
- [@kjellmf]'s awesome procedure that translates `.svg` files to tikz code.


[traditional types maps]: https://bulbapedia.bulbagarden.net/wiki/Type/Type_chart
[`pdflatex`]: http://www.tug.org/texlive/
[@kjellmf]: https://github.com/kjellmf/svg2tikz
[@duiker101]: https://github.com/duiker101/pokemon-type-svg-icons
[tikz]: https://texample.net/tikz/
