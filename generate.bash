#!/bin/bash -e

initFolder=$(pwd)

# Translate svg type icons into tikz paths.
svgFolder="pokemon-type-svg-icons/icons" # Cheers @duiker101!
translationScriptFolder="svg2tikz/scripts" # Cheers @kjellmf!
tzFolder="tz_icons"
srcFolder="src"
outputFolder="output"

mkdir -p $initFolder/$tzFolder
mkdir -p $initFolder/$outputFolder

# Translate every file.
# For some reason, the script only works from this very folder.
cd $translationScriptFolder
for iconFile in $(ls $initFolder/$svgFolder); do
    echo "Translating $iconFile to a tikz path.."
    ./svg2tikz $initFolder/$svgFolder/$iconFile \
        `# Filter out wrapper LaTeX commands to only keep the \path lines.` \
        | grep "\\path" \
        `# Replace the default filling with our custom style.` \
        | perl -pe "s/fill=.*?,/type path,/g" \
        `# This is correct output for use in our LaTeX source.` \
        > $initFolder/$tzFolder/${iconFile%.*}.tz
done
# A few tweaks for shorter names.
cd $initFolder/$tzFolder
mv electric.tz elec.tz
mv fighting.tz fight.tz
mv flying.tz   fly.tz
mv psychic.tz  psy.tz

echo "Compiling document.."
cd $initFolder/$srcFolder
pdflatex main.tex

# Extract result and we're done.
mv $initFolder/$srcFolder/main.pdf $initFolder/$outputFolder

cd $initFolder
printf "\nDone.\n"
exit 0
