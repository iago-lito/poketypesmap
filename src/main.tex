\documentclass{article}
\usepackage[a4paper,margin=1cm, landscape]{geometry}
\usepackage{xcolor}
\usepackage{xsavebox}
\usepackage{tikz}
    \usetikzlibrary{calc}
    \usetikzlibrary{positioning}
    \usetikzlibrary{fadings}
    \usetikzlibrary{patterns}
    \usetikzlibrary{arrows}
\graphicspath{{icons/}}
\input{apply.tex}
% =============================================================================
% Some tweakable parameters.

% Global size scale, coordinates are expressed with this unit.
\newcommand{\unit}{1.4mm}
\tikzset{x=\unit, y=\unit}
% Hexagonal summary radius.
\newcommand{\SummaryRadius}{12}
% Space between two summaries centers.
\newcommand{\SummarySeparation}{35}

% Basic colors.
\definecolor{page}{gray}{1.0}
\definecolor{bg}{gray}{0.7} % Background for main types.
\definecolor{text}{gray}{0.3}

% Optional shading.
\usepackage{ifthen}
\newboolean{shade}
% Set to false to remove shadings and speed up rendering.
\setboolean{shade}{true}

% Official color palette, I.. think?
\apply[#1 = #2]{
    \definecolor{#1}{HTML}{#2}
    % Improve compile time with saveboxes.
    \xsavebox{#1Box}{
        \tikz[type path/.style={draw, fill=#1, scale=.019, y=-\unit}]
            {\input{icons/#1.tz}}
    }
}{
    bug    = 84BD5A,
    dark   = 5C5564,
    dragon = 0075B7,
    elec   = FFCA69,
    fairy  = FD8DDB,
    fight  = E93468,
    fire   = FF9268,
    fly    = 7DACD4,
    ghost  = 3B6EA3,
    grass  = 2CBA6E,
    ground = F36D55,
    ice    = 26CEBF,
    normal = 8B999F,
    poison = B36EBD,
    psy    = FF647A,
    rock   = CCB393,
    steel  = 35909D,
    water  = 0096CB,
}

% =============================================================================
% Now the technical stuff is starting.

% Color the page.
\pagecolor{page}

% Use a precompiled type icon.
\newcommand{\pktype}[1]{\xusebox{#1Box}}

% Define key anchor points within each summary.
\newcommand{\SummaryCoordinates}{
    % Key points.
    \coordinate (O) at (0,0);
    \coordinate (W) at (0:\SummaryRadius);
    \coordinate (E) at (180:\SummaryRadius);
    \coordinate (NW) at (60:\SummaryRadius);
    \coordinate (SW) at (-60:\SummaryRadius);
    \coordinate (NE) at (120:\SummaryRadius);
    \coordinate (SE) at (-120:\SummaryRadius);
    % Key unit vectors.
    \coordinate (w) at (0:\SummaryRadius/6);
    \coordinate (e) at (180:\SummaryRadius/6);
    \coordinate (nw) at (60:\SummaryRadius/6);
    \coordinate (sw) at (-60:\SummaryRadius/6);
    \coordinate (ne) at (120:\SummaryRadius/6);
    \coordinate (se) at (-120:\SummaryRadius/6);
    % Further outer points useful for shading.
    \def\farOut{1.5}
    \foreach \i/\j in {%
        fw/W,%
        fnw/NW,%
        fsw/SW,%
        fe/E,%
        fne/NE,%
        fse/SE%
    } {
        \coordinate (\i) at ($(O)!\farOut!(\j)$);
    }
}
% Shadings in the background of each summary.
\tikzfading[name=fade out,
            inner color=transparent!00,
            outer color=transparent!100]
\newcommand{\BackgroundShades}{
    % Positive side.
    \begin{scope}
        \clip (fw) -- (fnw) -- (fse) -- (fsw) -- cycle;
        \path[fill=green!70!black, path fading=fade out]
            (O) circle (.85*\farOut*\SummaryRadius);
    \end{scope}
    % Negative side.
    \begin{scope}
        \clip (fne) -- (fnw) -- (fse) -- (fe) -- cycle;
        \path[fill=red!70!black, path fading=fade out]
            (O) circle (.85*\farOut*\SummaryRadius);
    \end{scope}
    % Attack side.
    \begin{scope}
        \clip (fe) -- (fne) -- (fnw) -- (fw) -- cycle;
        \path[path fading=fade out]
            (O) circle (.85*\farOut*\SummaryRadius);
    \end{scope}
    % Defense side.
    \begin{scope}
        \clip (fe) -- (fse) -- (fsw) -- (fw) -- cycle;
        \path[pattern=dots, path fading=fade out]
            (O) circle (.85*\farOut*\SummaryRadius);
    \end{scope}
    % Null cones.
    \begin{scope}
        \clip (fnw) -- (fne) -- (fsw) -- (fse) -- cycle;
        \path[fill=black!40!white, path fading=fade out]
            (O) circle (.85*\farOut*\SummaryRadius);
    \end{scope}
}

% Draw the background for one summary.
\newcommand{\Background}{
    \SummaryCoordinates

    % Now draw.
    \ifthenelse{\boolean{shade}}{\BackgroundShades}{}

    % One hexagon as the summary background.
    \path[fill, bg] (E) -- (NE) -- (NW) --
                    (W) -- (SW) -- (SE) -- cycle;

    % Label the directions.
    \newcommand{\Label}[4]{
        \coordinate (anchor) at ($(##2)!.5!(##3)$);
        \node[rotate=##1, scale=.7, color=text] () at ($(O)!.73!(anchor)$)
            {\textbf{##4}};
    }
    \Label{-60}{NW}{W}{efficient}
    \Label{60}{NE}{E} {inefficient}
    \Label{-60}{SE}{E}{weak}
    \Label{60}{SW}{W} {resistant}
    \Label{0}{NW}{NE} {useless}
    \Label{0}{SW}{SE} {immune}

}
% Build up small dedicated syntax to ease setting of the types:
%   i-j: type, % With i, j coordinates on the triangular grid.
\newcommand{\placeType}[6]{
    \node[scale=1/3] () at ($(#1) + #2*(#3) + #4*(#5)$) {\pktype{#6}};
}
\newcommand{\placeTypes}[4]{
    \apply[##1-##2: ##3]{\placeType{#1}{##1}{#2}{##2}{#3}{##3}}{#4}
}

% Note that the grid orientation is not the same depending on the direction.
\newcommand{\StrongAttack}[1]{\placeTypes{NW}{sw}{w}{#1}}
\newcommand{\WeakAttack}[1]{\placeTypes{NE}{se}{e}{#1}}
\newcommand{\WeakDefense}[1]{\placeTypes{SE}{ne}{e}{#1}}
\newcommand{\StrongDefense}[1]{\placeTypes{SW}{nw}{w}{#1}}
\newcommand{\NullAttack}[1]{\placeTypes{NE}{w}{nw}{#1}}
\newcommand{\NullDefense}[1]{\placeTypes{SE}{w}{sw}{#1}}

% Build 1 summary.
\newcommand{\TypeSummary}[2]{
    \begin{tikzpicture}
    \Background
    \node (main) at (O) {\pktype{#1}};
    #2
    \end{tikzpicture}
}
% Locate 1 summary.
\newcommand{\placeSummary}[3]{
    \node (#3) at ($#1*(u) + #2*(v)$) {\csname#3Summary\endcsname};
}
% Sketch a small legend under the form of a special summary.
\newcommand{\legendSummary}{
\begin{tikzpicture}[scale=.7]
    \SummaryCoordinates
    \BackgroundShades
    \node[red!80!black, rotate=60,anchor=center] at (150:.85*\SummaryRadius)
        {\textbf{weaknesses}};
    \node[green!50!black, rotate=60,anchor=center] at (-30:.85*\SummaryRadius)
        {\textbf{strengths}};
    \node[anchor=center] at (90:.3*\SummaryRadius) {\textbf{attack}};
    \node[anchor=center] at (-90:.3*\SummaryRadius) {\textbf{defense}};
\end{tikzpicture}
}

% All technical stuff has been defined.

% =============================================================================
% The types affinity details starts here.
% The data is entangled with positioning,
% to allow easy hand tweaks when the spacing feels weird.
% Positionning units are coordinates on a triangular grid,
% so the positionning remains consistent with the hexagonal organization.

\newcommand{\bugSummary}{\TypeSummary{bug}{

\StrongAttack{%
    1-0: psy,
    3-0: grass,
    5-0: dark,
}

\WeakAttack{%
    0-4: fairy,
    2-0: fly,
    4-0: poison,
    0-2: ghost,
    2-2: fire,
    2-4: steel,
    4-2: fight,
}

\WeakDefense{%
    1-0: rock,
    3-0: fire,
    5-0: fly,
}

\StrongDefense{%
    1-0: fight,
    3-0: grass,
    5-0: ground,
}

}}
\newcommand{\darkSummary}{\TypeSummary{dark}{

\StrongAttack{%
    2-0: psy,
    4-0: ghost,
}

\WeakAttack{%
    1-0: dark,
    3-0: fairy,
    5-0: fight,
}

\WeakDefense{%
    1-0: fight,
    3-0: fairy,
    5-0: bug,
}

\StrongDefense{%
    2-0: ghost,
    4-0: dark,
}

\NullDefense{%
    3-0: psy,
}

}}
\newcommand{\dragonSummary}{\TypeSummary{dragon}{

\StrongAttack{%
    3-0: dragon,
}

\WeakAttack{%
    3-0: steel,
}

\NullAttack{%
    3-0: fairy,
}

\WeakDefense{%
    1-0: fairy,
    3-0: dragon,
    5-0: ice,
}

\StrongDefense{%
    1-0: water,
    3-0: fire,
    5-0: elec,
    1-2: grass,
}

}}
\newcommand{\elecSummary}{\TypeSummary{elec}{

\StrongAttack{%
    2-0: fly,
    4-0: water,
}

\WeakAttack{%
    2-0: dragon,
    4-0: elec,
}

\WeakDefense{%
    3-0: ground,
}

\StrongDefense{%
    1-0: elec,
    3-0: steel,
    5-0: fly,
}

\NullAttack{%
    3-0: ground,
}

}}
\newcommand{\fairySummary}{\TypeSummary{fairy}{

\StrongAttack{%
    1-0: dragon,
    3-0: dark,
    5-0: fight,
}

\WeakAttack{%
    1-0: steel,
    3-0: fire,
    5-0: poison,
}

\WeakDefense{%
    2-0: poison,
    4-0: steel,
}

\StrongDefense{%
    1-0: fight,
    3-0: dark,
    5-0: bug,
}

\NullDefense{%
    3-0: dragon,
}



}}
\newcommand{\fightSummary}{\TypeSummary{fight}{

\StrongAttack{%
    1-0: dark,
    3-0: rock,
    5-0: steel,
    1-2: ice,
    3-2: normal,
}

\WeakAttack{%
    1-0: fly,
    3-0: fairy,
    5-0: bug,
    1-2: psy,
    3-2: poison,
}

\WeakDefense{%
    2-0: fairy,
    4-0: fly,
}

\StrongDefense{%
    1-0: bug,
    3-0: rock,
    5-0: dark,
}

\NullAttack {%
    3-0: ghost,
}

\NullDefense {%
    3-0: ghost,
}

}}
\newcommand{\fireSummary}{\TypeSummary{fire}{

\StrongAttack{%
    1-0: bug,
    3-0: grass,
    5-0: ice,
    1-2: steel,
}

\WeakAttack{%
    1-0: rock,
    3-0: water,
    5-0: dragon,
    3-2: fire,
}

\StrongDefense{%
    1-0: ice,
    3-0: grass,
    5-0: bug,
    1-2: fire,
    3-2: steel,
    1-4: fairy,
}

\WeakDefense{%
    1-0: ground,
    3-0: water,
    5-0: rock,
}

}}
\newcommand{\flySummary}{\TypeSummary{fly}{

\StrongAttack{%
    1-0: bug,
    3-0: grass,
    5-0: fight,
}

\WeakAttack{%
    1-0: steel,
    3-0: elec,
    5-0: rock,
}

\WeakDefense{%
    1-0: rock,
    3-0: elec,
    5-0: ice,
}

\StrongDefense{%
    1-0: fight,
    3-0: grass,
    5-0: bug,
}

\NullDefense {%
    3-0: ground,
}

}}
\newcommand{\ghostSummary}{\TypeSummary{ghost}{

\StrongAttack{%
    2-0: psy,
    4-0: ghost,
}

\WeakAttack{%
    3-0: dark,
}

\NullAttack{%
    3-0: normal,
}

\WeakDefense{%
    2-0: ghost,
    4-0: dark,
}

\StrongDefense{%
    2-0: poison,
    4-0: bug,
}

\NullDefense{%
    2-0: normal,
    4-0: fight,
}

}}
\newcommand{\grassSummary}{\TypeSummary{grass}{

\StrongAttack{%
    1-0: ground,
    3-0: water,
    5-0: rock,
}

\WeakAttack{%
    0-4: grass,
    2-0: fly,
    4-0: bug,
    0-2: poison,
    2-2: fire,
    2-4: steel,
    4-2: dragon,
}

\WeakDefense{%
    1-0: ice,
    3-0: fire,
    5-0: bug,
    1-2: poison,
    3-2: fly,
}

\StrongDefense{%
    1-0: elec,
    3-0: water,
    5-0: ground,
    3-2: grass,
}

}}
\newcommand{\groundSummary}{\TypeSummary{ground}{

\StrongAttack{%
    1-0: rock,
    3-0: elec,
    5-0: fire,
    1-2: poison,
    3-2: steel,
}

\WeakAttack{%
    2-0: grass,
    4-0: bug,
}

\NullAttack{%
    3-0: fly,
}

\WeakDefense{%
    1-0: ice,
    3-0: water,
    5-0: grass,
}

\StrongDefense{%
    2-0: poison,
    4-0: rock,
}

\NullDefense{%
    3-0: elec,
}

}}
\newcommand{\iceSummary}{\TypeSummary{ice}{

\StrongAttack{%
    1-0: dragon,
    3-0: grass,
    5-0: ground,
    3-2: fly,
}

\WeakAttack{%
    1-0: water,
    3-0: fire,
    5-0: steel,
    3-2: ice,
}

\WeakDefense{%
    1-0: steel,
    3-0: fire,
    5-0: rock,
    1-2: fight,
}

\StrongDefense{%
    3-0: ice,
}

}}
\newcommand{\normalSummary}{\TypeSummary{normal}{

\WeakAttack{%
    2-0: rock,
    4-0: steel,
}

\WeakDefense{%
    3-0: fight,
}

\NullAttack{%
    3-0: ghost,
}

\NullDefense{%
    3-0: ghost,
}

}}
\newcommand{\poisonSummary}{\TypeSummary{poison}{

\StrongAttack{%
    2-0: fairy,
    4-0: grass,
}

\WeakAttack{%
    1-0: ghost,
    3-0: rock,
    5-0: ground,
    3-2: poison,
}

\NullAttack{%
    3-0: steel,
}

\WeakDefense{%
    2-0: ground,
    4-0: psy,
}

\StrongDefense{%
    1-0: fairy,
    3-0: grass,
    5.1-0: bug,
    1-2: fight,
    3-2: poison,
}

}}
\newcommand{\psySummary}{\TypeSummary{psy}{

\StrongAttack{%
    2-0: poison,
    4-0: fight,
}

\WeakAttack{%
    2-0: psy,
    4-0: steel,
}

\NullAttack{%
    3-0: dark,
}

\WeakDefense{%
    1-0: dark,
    3-0: ghost,
    5-0: bug,
}

\StrongDefense{%
    2-0: fight,
    4-0: psy,
}

}}
\newcommand{\rockSummary}{\TypeSummary{rock}{

\StrongAttack{%
    1-0: fly,
    3-0: fire,
    5-0: ice,
    3-2: bug,
}

\WeakAttack{%
    1-0: ground,
    3-0: steel,
    5-0: fight,
}

\WeakDefense{%
    1-0: fight,
    3-0: steel,
    5-0: ground,
    1-2: grass,
    3-2: water,
}

\StrongDefense{%
    1-0: normal,
    3-0: fire,
    5-0: fly,
    3-2: poison,
}


}}
\newcommand{\steelSummary}{\TypeSummary{steel}{

\NullDefense{
    3-0: poison,
}

\StrongAttack{
  1-0: ice,
  3-0: rock,
  5-0: fairy,
}

\WeakAttack{
  1-0: water,
  3-0: fire,
  5-0: elec,
  3-2: steel,
}

\WeakDefense{
  1-0: fight,
  3-0: fire,
  5-0: ground,
}

\StrongDefense{
  1-0: fairy,
  3-0: rock,
  5-0: ice,
  1-2: grass,
  3-2: bug,
  1-4: normal,
  5-2: steel,
  3-4: psy,
  {-1}-2: dragon,
  {-1}-4: fly,
}

}}
\newcommand{\waterSummary}{\TypeSummary{water}{

\StrongAttack{
  1-0: fire,
  3-0: ground,
  5-0: rock,
}

\WeakAttack{
  1-0: grass,
  3-0: dragon,
  5-0: water,
}

\WeakDefense{
  2-0: elec,
  4-0: grass,
}

\StrongDefense{
  1-0: ice,
  3-0: steel,
  5-0: fire,
  3-2: water,
}

}}

% All types affinities have been defined.
% =============================================================================

\begin{document}

\thispagestyle{empty}

% Brutal horizontal and vertical centering.
\null\vfill%
\noindent\null\hfill%
%
% Now locate the various summaries on a bigger, vertical hexagonal grid.
\begin{tikzpicture}
\coordinate (u) at (-30:\SummarySeparation);
\coordinate (v) at (-150:\SummarySeparation);

% Same kind of small, dedicated syntax to ease positionning.
\apply[#1/#2: #3]{\placeSummary{#1}{#2}{#3}}{%
%
    -0.80/0.20: legend,
%
    0/1: fire,
    0/0: grass,
    1/0: water,
% %
    1/2: elec,
    1/1: normal,
    2/1: ice,
% %
    2/3: ground,
    2/2: rock,
    3/2: steel,
% %
    1/-1: fly,
    2/-1: bug,
    2/-2: dragon,
% %
    2/0:  poison,
    3/0:  psy,
    3/-1: fight,
% %
    3/1: ghost,
    4/1: dark,
    4/0: fairy,
%
}

\end{tikzpicture}%
\hfill\null\vfill\null%

\end{document}

